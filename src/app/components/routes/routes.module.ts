import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { ContactoComponent } from './contacto/contacto.component';
import { PromocionesComponent } from './promociones/promociones.component';



@NgModule({
  declarations: [
    InicioComponent,
    ServiciosComponent,
    ContactoComponent,
    PromocionesComponent,
  ],
  imports: [
    CommonModule
  ]
})
export class RoutesModule { }
